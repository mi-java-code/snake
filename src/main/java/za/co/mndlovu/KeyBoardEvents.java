package za.co.mndlovu;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyBoardEvents extends KeyAdapter {
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;

    public void move() {

    }

    public void move(int x[], int y[], int dots, int dotSize) {

        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }
        if (left) {
            x[0] -= dotSize;
        }
        else if (right) {
            x[0] += dotSize;
        }
        else if (up) {
            y[0] -= dotSize;
        }
        else if (down) {
            y[0] += dotSize;
        }
    }


    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if ((key == KeyEvent.VK_LEFT || key == KeyEvent.VK_4) && (!right && key != KeyEvent.VK_6)) {
            left = true;
            up = false;
            down = false;
        } else if ((key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_6) && (!left && key != KeyEvent.VK_4)) {
            right = true;
            up = false;
            down = false;
        } else if ((key == KeyEvent.VK_UP || key == KeyEvent.VK_2) && (!down && key != KeyEvent.VK_8)) {
            up = true;
            right = false;
            left = false;
        } else if ((key == KeyEvent.VK_DOWN || key == KeyEvent.VK_8) && (!up && key != KeyEvent.VK_2)) {
            down = true;
            right = false;
            left = false;
        }
    }
}

