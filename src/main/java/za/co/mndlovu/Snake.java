package za.co.mndlovu;

import java.awt.*;
import javax.swing.*;

public class Snake extends JFrame{

    public static void main(String[] args) {

        Image icon = new ImageIcon("/home/mbongeni/private/project/java/Snake/src/main/resources/snake.jpg").getImage();
        JFrame ex = new JFrame("Snake");
        ex.add(new Board());
        ex.setResizable(false);
        ex.pack();
        ex.setLocationRelativeTo(null);
        ex.setSize(415,345);
        ex.setIconImage(icon);
        ex.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ex.setVisible(true);
    }

}
