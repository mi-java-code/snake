package za.co.mndlovu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.xml.crypto.dsig.XMLObject;

public class Board extends JPanel implements ActionListener {

    private static final int boardWidth = 300;
    private static final int boardHeight = 300;
    private static final int x[] = new int[900];
    private static final int y[] = new int[900];
    private int dots;
    private int foodX;
    private int foodY;
    private boolean withinBoardRange = true;
    private Timer timer;
    private KeyBoardEvents keyBoardEvents;
    private Image apple;
    private int points = 0;

    public Board() {
        initBoard();
    }

    private void initBoard() {
        keyBoardEvents = new KeyBoardEvents();
        addKeyListener(keyBoardEvents);
        setBackground(Color.BLACK);
        setFocusable(true);
        setPreferredSize(new Dimension(boardWidth, boardHeight));
        loadImage();
        initGame();
    }


    private void initGame() {
        dots = 3;

        for (int z = 0; z < dots; z++) {
            x[z] = 50 - z * 10;
            y[z] = 50;
        }
        locateFood();
        timer = new Timer(150, this);
        timer.start();
    }

    private void locateFood() {
        int randPos = 29;
        int dotSize = 10;
        int rand = (int) (Math.random() * randPos);
        foodX = rand * dotSize;
        if (foodX < 10) {
            foodX += 10;
        } else if (foodX > 290) {
            foodX -= 10;
        }
        rand = (int) (Math.random() * randPos);
        foodY = ((rand * dotSize));
        if (foodY < 10) {
            foodY += 10;
        } else if (foodY > 290) {
            foodX -= 10;
        }
    }

    private void loadImage() {
        System.out.println(System.getProperty("user.dir"));
        ImageIcon iia = new ImageIcon("/home/mbongeni/private/project/java/Snake/src/main/resources/apple.png");
        apple = iia.getImage();
    }

    // It paints and repaints the frame

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawASnake(g);
        drawSideInfo(g);
    }

    private void drawSideInfo(Graphics g) {
        g.setColor(Color.orange);
        g.fillRect(0,0,10,310);
        g.fillRect(0,0,410,10);
        g.fillRect(0,300,410,10);
        g.fillRect(300, 0, 10, 310);
        g.fillRect(400, 0, 10, 310);
        g.fillRect(300, 60, 100, 10);
        Font small = new Font("Helvetica", Font.BOLD, 16);
        g.setFont(small);
        g.drawString("Points", 320, 30);
        g.drawString(Integer.toString(points), 320,50);
    }

    private void drawASnake(Graphics g) {
        if (withinBoardRange) {
            g.drawImage(apple,foodX, foodY, this);
            g.setColor(Color.GREEN);
            for (int z = 0; z < dots; z++) {
                if (z == 0) {
                    g.drawRect(x[z],y[z],9,9);
                } else {
                    g.fillRect(x[z],y[z],10,10);
                }
            }
        } else {
            gameOver(g);
        }
    }

    /**TODO fix this boy
     * top == 0  -20
     * left == 0  -20
     * bottom == 260 310
     * right == 290 310
     * @param g
     */

    private void gameOver(Graphics g) {
        String msg = "Game Over";
//        String finalPoints = Integer.toString(points).concat(" points");
//        Font small = new Font("Helvetica", Font.BOLD, 16);
//        g.setFont(small);
        g.setColor(Color.GREEN);
        // g.drawString(finalPoints, 110,100);
        Font big = new Font("Helvetica", Font.BOLD, 30);
        g.setFont(big);
        g.drawString(msg, 70 , boardHeight / 2);
//        System.exit(0);
    }

    private void eatFood() {
        if ((x[0] == foodX) && (y[0] == foodY)) {
            dots++;
            points += 15;
            locateFood();
        }
    }


    private void checkCollision() {
        for (int z = dots; z > 0; z--) {
            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                withinBoardRange = false;
            }
        }
        if ((y[0] >= boardHeight) || (y[0] <= 0) || (x[0] >= boardWidth) || (x[0] <= 0)) {
            withinBoardRange = false;
        }
        if (!withinBoardRange) {
            timer.stop();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (withinBoardRange) {
            eatFood();
            checkCollision();
            keyBoardEvents.move(x,y,dots,10);
        }
        repaint();
    }
}

